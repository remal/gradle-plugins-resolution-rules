import Build_gradle.CapabilityQuery.*
import Build_gradle.Generator
import com.google.common.io.CountingInputStream
import de.undercouch.gradle.tasks.download.Download
import name.remal.arrayEquals
import name.remal.buildList
import name.remal.concurrentMapOf
import name.remal.concurrentSetOf
import name.remal.createParentDirectories
import name.remal.escapeRegex
import name.remal.forceDelete
import name.remal.forceDeleteRecursively
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.artifact.HasEntries
import name.remal.gradle_plugins.dsl.extensions.compileOnly
import name.remal.gradle_plugins.dsl.extensions.convention
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.forTemp
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isSnapshotVersion
import name.remal.gradle_plugins.dsl.extensions.makeNotTransitive
import name.remal.gradle_plugins.dsl.extensions.newTempFile
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsExtension
import name.remal.gradle_plugins.plugins.publish.bintray.RepositoryHandlerBintrayExtension
import name.remal.gradle_plugins.plugins.publish.nexus_staging.ReleaseNexusStagingRepository
import name.remal.gradle_plugins.plugins.publish.ossrh.RepositoryHandlerOssrhExtension
import name.remal.gradle_plugins.plugins.vcs.VcsOperations
import name.remal.json.data_format.DataFormatJSON.JSON_DATA_FORMAT
import name.remal.json.readJson
import name.remal.json.writeJsonTo
import name.remal.nullIf
import name.remal.nullIfEmpty
import name.remal.reflection.TypeProvider
import name.remal.retry
import name.remal.uncheckedCast
import name.remal.use
import name.remal.version.Version
import org.apache.maven.index.reader.ChunkReader
import org.apache.maven.index.reader.Record.ARTIFACT_ID
import org.apache.maven.index.reader.Record.CLASSIFIER
import org.apache.maven.index.reader.Record.FILE_EXTENSION
import org.apache.maven.index.reader.Record.GROUP_ID
import org.apache.maven.index.reader.Record.VERSION
import org.apache.maven.index.reader.RecordExpander
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.PathSensitivity.RELATIVE
import org.gradle.initialization.BuildCancellationToken
import org.jsoup.HttpStatusException
import org.jsoup.Jsoup
import java.io.IOException
import java.lang.System.currentTimeMillis
import java.lang.System.getenv
import java.lang.System.nanoTime
import java.net.HttpURLConnection
import java.net.URI
import java.net.URL
import java.time.Duration
import java.util.Collections.synchronizedSortedMap
import java.util.SortedMap
import java.util.SortedSet
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong
import javax.inject.Inject
import kotlin.collections.set
import kotlin.math.floor
import kotlin.text.Regex
import kotlin.text.isNotEmpty
import name.remal.gradle_plugins.dsl.artifact.Artifact as RemalArtifact

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("name.remal:gradle-plugins:1.1.1")
        classpath("name.remal.tools:json:1.26.147")
        classpath("de.undercouch:gradle-download-task:4.1.1")
        classpath("org.apache.maven.indexer:indexer-reader:6.0.0")
        classpath("org.jsoup:jsoup:1.13.1")
        classpath("com.google.guava:guava:29.0-jre")
    }
}

plugins {
    java
    `maven-publish`
}


apply(plugin = "name.remal.default-plugins")
apply(plugin = "name.remal.gitlab-ci")
apply(plugin = "name.remal.vcs-operations")

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

val sourcesDir = file("src")
val resourcesDir = file("$sourcesDir/main/resources")
val buildCacheDir = file("build-cache")
val buildCommittedCacheDir = file("build-cache-committed")

repositories {
    mavenCentral()
    maven { setUrl("https://oss.sonatype.org/service/local/repositories/releases/content/") }
}

project.group = "name.remal"
project.version = "0-SNAPSHOT"

val vcsOperations = project[VcsOperations::class.java]
logger.lifecycle("Current VCS branch: {}", vcsOperations.currentBranch)
if (vcsOperations.currentBranch == vcsOperations.masterBranch) {
    val projectNotation = "$group:$name"
    val dependencyVersions = project[DependencyVersionsExtension::class.java]
    val prevVersion = try {
        Version.parse(dependencyVersions.resolveLatestVersion(projectNotation))
    } catch (e: Exception) {
        if (dependencyVersions.resolveAllVersions(projectNotation).isEmpty()) {
            logger.warn(
                "{}: latest version couldn't be retrieved: {}",
                projectNotation,
                e.toString()
            )
            null
        } else {
            throw e
        }
    }

    if (prevVersion == null) {
        project.version = "0.1"

    } else {
        val currentVersion = prevVersion.incrementNumber(prevVersion.numbersCount - 1)
        project.version = currentVersion.toString()

        tasks.withType<AbstractPublishToMaven> {
            dependsOn("build")

            onlyIf {
                val prevArtifactFile: File = project.repositories.asSequence()
                    .filterIsInstance(MavenArtifactRepository::class.java)
                    .mapNotNull(MavenArtifactRepository::getUrl)
                    .filter { it.scheme == "http" || it.scheme == "https" }
                    .map(URI::toString)
                    .map {
                        URL(
                            arrayOf(
                                it.trimEnd('/'),
                                publication.groupId.replace('.', '/'),
                                publication.artifactId,
                                prevVersion,
                                "${publication.artifactId}-$prevVersion.jar"
                            ).joinToString("/")
                        )
                    }
                    .mapNotNull { url ->
                        retry(10, IOException::class.java, 2_500) {
                            val connection: HttpURLConnection = url.openConnection().uncheckedCast()
                            connection.connectTimeout = 2_500
                            connection.readTimeout = 5_000
                            connection.useCaches = false
                            connection.instanceFollowRedirects = true
                            connection.requestMethod = "GET"
                            connection.connect()
                            try {
                                when (val responseCode = connection.responseCode) {
                                    200 -> {
                                        val tempFile = newTempFile("${publication.artifactId}-$prevVersion-", ".jar")
                                        tempFile.outputStream().use { outputStream ->
                                            connection.inputStream.use { inputStream ->
                                                inputStream.copyTo(outputStream)
                                            }
                                        }

                                        gradle.buildFinished { tempFile.delete() }

                                        return@mapNotNull tempFile
                                    }

                                    404 -> {
                                        logger.debug("{} {} - 404 Not Found", connection.requestMethod, url)
                                        return@mapNotNull null
                                    }

                                    else -> throw IOException("${connection.requestMethod} $url returned $responseCode status")
                                }

                            } finally {
                                connection.disconnect()
                            }
                        }
                    }
                    .firstOrNull()
                    ?: throw IllegalStateException("Previous artifact can't be found in project Maven repositories")
                val prevArtifact = RemalArtifact(prevArtifactFile)
                val prevEntryNames = prevArtifact.entryNames.toSortedSet().filter { !it.startsWith("META-INF/") }

                val currentArtifact = publication.artifacts.first { it.classifier.isNullOrEmpty() && it.extension == "jar" }.file.let(
                    ::RemalArtifact
                )
                val currentEntryNames = currentArtifact.entryNames.toSortedSet().filter { !it.startsWith("META-INF/") }

                val changes = sortedMapOf<String, EntryStatus>()
                prevEntryNames.toMutableList()
                    .apply { removeAll(currentEntryNames) }
                    .forEach { changes[it] = EntryStatus.DELETED }
                currentEntryNames.toMutableList()
                    .apply { removeAll(prevEntryNames) }
                    .forEach { changes[it] = EntryStatus.ADDED }
                prevEntryNames.intersect(currentEntryNames).forEach { entryName ->
                    val prevBytes = prevArtifact.readBytes(entryName)
                    val currentBytes = currentArtifact.readBytes(entryName)
                    if (!prevBytes.arrayEquals(currentBytes)) {
                        changes[entryName] = EntryStatus.CHANGED
                    }
                }
                if (changes.isEmpty()) {
                    logger.lifecycle(
                        "Content of {} and {} (current) versions is equal. Skipping publishing.",
                        prevVersion,
                        currentVersion
                    )
                    return@onlyIf false

                } else {
                    logger.lifecycle("Comparing content of versions {} and {} (current):", prevVersion, currentVersion)
                    changes.forEach { entry, status ->
                        logger.lifecycle("    [{}] {}", status.toPaddedString(), entry)
                    }
                }

                return@onlyIf true
            }
        }
    }
}

logger.lifecycle("Version: {}", project.version)

private enum class EntryStatus {
    DELETED,
    ADDED,
    CHANGED,
    ;

    fun toPaddedString() = buildString {
        val name = name.toLowerCase()
        append(name)
        repeat(values().map { it.name.length }.max()!! - name.length) {
            append(' ')
        }
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

val downloadMavenCentralRepositoryIndex = tasks.register<Download>("downloadMavenCentralRepositoryIndex") {
    src("https://repo1.maven.org/maven2/.index/nexus-maven-repository-index.gz")
    dest(file("$buildCacheDir/maven-central-repository-index.gz"))
    onlyIfModified(true)
    connectTimeout(10_000)
    readTimeout(300_000)
    retries(3)

    if (getenv("CI") != "true" && getenv("GITLAB_CI") != "true") {
        onlyIf {
            !dest.isFile
                || dest.lastModified() < currentTimeMillis() - Duration.ofDays(60).toMillis()
        }
    }
}

val collectMavenCentralRepositoryNotations = tasks.register<CollectRepositoryNotations>("collectMavenCentralRepositoryNotations") {
    dependsOn(downloadMavenCentralRepositoryIndex)
    repositoryIndexFile = downloadMavenCentralRepositoryIndex.get().dest
    includeJsr = false

    if (getenv("CI") != "true" && getenv("GITLAB_CI") != "true") {
        onlyIf {
            !repositoryIndexFile.isFile
                || repositoryIndexFile.lastModified() < currentTimeMillis() - Duration.ofDays(60).toMillis()
                || !listFile.isFile
                || listFile.lastModified() < currentTimeMillis() - Duration.ofDays(60).toMillis()
        }
    }
}

tasks.register<GenerateJsonResources>("generateMavenCentralResources") {
    dependsOn(collectMavenCentralRepositoryNotations)
    tasks.processResources.get().dependsOn(this)

    destinationDir = resourcesDir.resolve("maven-central")

    val notations: List<String> by lazy {
        collectMavenCentralRepositoryNotations.get().listFile.readLines().asSequence()
            .map(String::trim)
            .filter(String::isNotEmpty)
            .distinct()
            .toList()
    }
    doSetup { property("notations", notations.joinToString("\n")) }

    val resolvedDependencies: Lazy<Collection<ResolvedDependency>> = lazy {
        configurations.forTemp { configuration ->
            configuration.makeNotTransitive()
            notations.forEach { notation ->
                val dependency = project.dependencies.create(notation)
                configuration.dependencies.add(dependency)
            }

            if (configuration.dependencies.isEmpty()) {
                return@lazy emptyList()
            }

            System.setProperty("plugin-disabled.name.remal.component-capabilities", "true")
            try {
                return@lazy configuration.resolvedConfiguration.lenientConfiguration.allModuleDependencies

            } finally {
                System.clearProperty("plugin-disabled.name.remal.component-capabilities")
            }
        }
    }

    if (collectMavenCentralRepositoryNotations.get().includeLogging) {
        generate(
            "logging-capabilities.json",
            CapabilitiesGenerator(
                "logging",
                resolvedDependencies,
                mapOf(
                    "slf4j-api" to listOf(ClassExistsCapabilityQuery("org.slf4j.LoggerFactory")),
                    "slf4j-impl" to listOf(ClassExistsCapabilityQuery("org.slf4j.impl.StaticLoggerBinder")),
                    "log4j-api" to listOf(ClassExistsCapabilityQuery("org.apache.logging.log4j.LogManager")),
                    "log4j-impl" to listOf(ServiceExistsCapabilityQuery("org.apache.logging.log4j.spi.Provider")),
                    "log4j12-api" to listOf(ClassExistsCapabilityQuery("org.apache.log4j.LogManager")),
                    "jcl-api" to listOf(ClassExistsCapabilityQuery("org.apache.commons.logging.LogFactory")),
                    "jcl-impl" to listOf(ServiceExistsCapabilityQuery("org.apache.commons.logging.LogFactory"))
                )
            )
        )
    }

    if (collectMavenCentralRepositoryNotations.get().includeJsr) {
        generate(
            "jsr-capabilities.json",
            CapabilitiesGenerator(
                "jsr",
                resolvedDependencies,
                mapOf(
                    "expression-language-api" to listOf(ClassExistsCapabilityQuery("javax.el.ExpressionFactory")),
                    "bean-validation-api" to listOf(ClassExistsCapabilityQuery("javax.validation.ValidatorFactory")),
                    "jsr205" to listOf(
                        ClassExistsCapabilityQuery("javax.annotation.Generated"),
                        ClassExistsCapabilityQuery("javax.annotation.security.RunAs")
                    ),
                    "jsr305" to listOf(
                        ClassExistsCapabilityQuery("javax.annotation.Nonnull"),
                        ClassExistsCapabilityQuery("javax.annotation.Nullable")
                    )
                )
            )
        )
    }
}

tasks.register("generateNebulaGradleResolutionRulesResources") {
    tasks.processResources.get().dependsOn(this)

    doLast {
        val destinationDir = resourcesDir.resolve("nebula-gradle-resolution-rules").forceDeleteRecursively()
        configurations.detachedConfiguration(project.dependencies.create("com.netflix.nebula:gradle-resolution-rules:+"))
            .makeNotTransitive()
            .files
            .let(::CachedArtifactsCollection)
            .also { artifact ->
                artifact.entryNames.asSequence()
                    .map { it.trim('/') }
                    .filter { !it.startsWith("META-INF/") }
                    .filter { it.endsWith(".json") }
                    .filter { !it.substringAfterLast('/').startsWith("optional-") }
                    .forEach { entryName ->
                        val file = destinationDir.resolve(entryName)
                        artifact.openStream(entryName).use { inputStream ->
                            file.createParentDirectories().outputStream().use { outputStream ->
                                inputStream.copyTo(outputStream)
                            }
                        }
                    }
            }

        didWork = true
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

publishing.publications.withType<MavenPublication> {
    groupId = project.group.toString()

    pom {
        name.set("Gradle resolution rules for Remal Gradle plugins")
        description.set(name.get())
        url.set("https://gitlab.com/remal/gradle-plugins-resolution-rules")
        scm {
            url.set("https://gitlab.com/remal/gradle-plugins-resolution-rules.git")
        }
        developers {
            developer {
                name.set("Semyon Levin")
                email.set("levin.semen@gmail.com")
                id.set(email.get())
            }
        }
        licenses {
            license {
                name.set("Unlicense")
                url.set("https://unlicense.org/UNLICENSE")
            }
        }
    }
}

val BINTRAY_ENABLED = false
val BINTRAY_USER = "remal"
val BINTRAY_PACKAGE = "name.remal"
if (BINTRAY_ENABLED) {
    publishing.repositories.convention[RepositoryHandlerBintrayExtension::class.java].bintray {
        owner = BINTRAY_USER
        repositoryName = "name.remal"
        packageName = BINTRAY_PACKAGE
    }
}

val OSSRH_ENABLED = true
if (OSSRH_ENABLED) {
    publishing.repositories.convention[RepositoryHandlerOssrhExtension::class.java].ossrh()
}

if (!OSSRH_ENABLED || project.isSnapshotVersion) {
    tasks.withType(ReleaseNexusStagingRepository::class.java).configureEach { disableTask() }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

typealias Generator = () -> Any

@CacheableTask
open class GenerateJsonResources : DefaultTask() {

    @OutputDirectory
    lateinit var destinationDir: File


    @Input
    @Optional
    val properties: SortedMap<String, String> = sortedMapOf()

    fun property(key: Any, value: Any?) {
        val unwrappedKey = key.unwrapProviders()?.toString() ?: return
        val unwrappedValue = value.unwrapProviders()?.toString()
        if (unwrappedValue != null) {
            properties[unwrappedKey] = unwrappedValue
        } else {
            properties.remove(unwrappedKey)
        }
    }


    private val generators: SortedMap<String, Generator> = sortedMapOf()

    fun generate(relativePath: String, generator: Generator) {
        generators[relativePath] = generator
    }


    @TaskAction
    protected fun doGenerate() {
        destinationDir.forceDeleteRecursively()

        generators.forEach { relativePath, generator ->
            if (buildCancellationToken.isCancellationRequested) throw BuildCancelledException()
            val targetFile = destinationDir.resolve(relativePath)
            val content = generator()
            content.writeJsonTo(targetFile.createParentDirectories(), JSON_DATA_FORMAT.withIndention())
        }

        didWork = true
    }


    @get:Input
    @get:Optional
    protected val generatedPaths: Collection<String>
        get() = generators.keys

    @get:InputFiles
    @get:Optional
    @get:PathSensitive(ABSOLUTE)
    protected val gradleFiles: Collection<File>
        get() = buildList {
            add(project.buildFile)
            project.rootDir.resolve("gradle.properties").nullIf { !isFile }?.let(::add)
            project.rootDir.resolve("settings.gradle").nullIf { !isFile }?.let(::add)
            project.rootDir.resolve("settings.gradle.kts").nullIf { !isFile }?.let(::add)
        }

    @get:Inject
    protected open val buildCancellationToken: BuildCancellationToken
        get() = TODO()

}

@CacheableTask
open class CollectRepositoryNotations : DefaultTask() {

    @Input
    @Optional
    protected val prohibitedChars = charArrayOf('+', ':', '*')

    protected class Matchers(
        private val groups: Iterable<String> = emptyList(),
        private val words: Iterable<String> = emptyList()
    ) {

        companion object {
            fun merge(vararg otherMatchers: Matchers?): Matchers {
                return Matchers(
                    otherMatchers.filterNotNull().flatMap { it.groups },
                    otherMatchers.filterNotNull().flatMap { it.words }
                )
            }
        }

        @Input
        @Optional
        val groupsList = groups.sorted()

        @Input
        @Optional
        val wordRegexps = words.asSequence()
            .flatMap { sequenceOf(it, it + "s") }
            .distinct()
            .sorted()
            .map(::escapeRegex)
            .map { "\\b$it\\d*(?:[._-]\\d*)*\\b" }
            .map(::Regex)
            .toList()

        fun matches(notation: DependencyNotation): Boolean {
            with(notation) {
                if (groupsList.any { group == it || (group.length > it.length && group.startsWith("$it.")) }) return true
                if (wordRegexps.any { it.containsMatchIn(group) }) return true
                if (wordRegexps.any { it.containsMatchIn(module) }) return true
                return false
            }
        }

    }

    @Nested
    protected val excludeMatchers = Matchers(
        listOf(
            "cn.6tail",
            "com.coralogix.sdk.appenders",
            "com.github.heavensay",
            "com.github.lzm320a99981e",
            "com.igormaznitsa",
            "com.imsweb",
            "com.jporm",
            "com.mastfrog",
            "groovy",
            "io.lighty.resources",
            "io.mathan.raml",
            "javax.jdo",
            "net.java.loci",
            "net.openhft",
            "net.sf.microlog",
            "no.nav.common",
            "org.apache.servicemix.tooling",
            "org.eobjects.datacleaner",
            "org.tinygroup.annotation",
            "org.webjars",
            "org.xeustechnologies"
        ),
        listOf(
            "archetype",
            "example",
            "help",
            "site"
        )
    )


    @Nested
    protected val loggingMatchers = Matchers(
        listOf(
            "ch.qos.logback",
            "commons-logging",
            "log4j",
            "org.apache.logging",
            "org.slf4j"
        ),
        listOf(
            "spring-jcl"
        )
    )

    @Input
    var includeLogging: Boolean = true

    @Nested
    protected val jsrMatchers = Matchers(
        listOf(
            "jakarta",
            "javax",
            "org.jboss.spec.jakarta",
            "org.jboss.spec.javax"
        ),
        listOf(
            "el",
            "jsr",
            "validation",
            "validator"
        )
    )

    @get:Input
    var includeJsr: Boolean = true

    @get:Nested
    protected val includeMatchers: Matchers
        get() = Matchers.merge(
            if (includeLogging) loggingMatchers else null,
            if (includeJsr) jsrMatchers else null,
            null
        )


    @get:InputFile
    @get:PathSensitive(RELATIVE)
    lateinit var repositoryIndexFile: File

    @get:OutputFile
    val listFile: File
        get() = repositoryIndexFile.resolveSibling("$name.result.txt")

    @TaskAction
    protected fun doCollect() {
        listFile.forceDelete()

        val percentageLogger = PercentageLogger(
            "Process repository index",
            repositoryIndexFile.length()
        )

        val versions = sortedMapOf<DependencyNotation, Version>()
        CountingInputStream(repositoryIndexFile.inputStream()).use stream@{ inputStream ->
            ChunkReader("full", inputStream).use reader@{ chunkReader ->
                val recordExpander = RecordExpander()
                chunkReader.forEach forEachRecord@{ info ->
                    if (buildCancellationToken.isCancellationRequested) throw BuildCancelledException()
                    percentageLogger.log(inputStream.count)

                    val record = recordExpander.apply(info)

                    if (!record[CLASSIFIER].isNullOrEmpty()) return@forEachRecord
                    if (record[FILE_EXTENSION] != "jar") return@forEachRecord

                    val group = record[GROUP_ID].nullIfEmpty() ?: return@forEachRecord
                    if (prohibitedChars.any { group.contains(it) }) return@forEachRecord
                    val module = record[ARTIFACT_ID].nullIfEmpty() ?: return@forEachRecord
                    if (prohibitedChars.any { module.contains(it) }) return@forEachRecord
                    val notation = DependencyNotation(group, module)
                    if (excludeMatchers.matches(notation)) return@forEachRecord

                    if (includeMatchers.matches(notation)) {
                        val version = record[VERSION].nullIfEmpty()
                            .nullIf { project[DependencyVersionsExtension::class.java].getFirstInvalidToken(this) != null }
                            ?.let(Version::parseOrNull)
                            ?: return@forEachRecord

                        val prevVersion = versions[notation]
                        if (prevVersion == null || prevVersion < version) {
                            versions[notation] = version
                        }
                    }
                }
            }
        }

        listFile.createParentDirectories().writeText(
            versions.asSequence()
                .map { it.key.withVersion(it.value.toString()).withoutClassifier().withExtension("jar") }
                .distinct()
                .sorted()
                .joinToString("\n")
        )

        didWork = true
    }

    @get:Inject
    protected open val buildCancellationToken: BuildCancellationToken
        get() = TODO()

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

sealed class CapabilityQuery {

    abstract val condition: HasEntries.() -> Boolean

    data class ClassExistsCapabilityQuery(val className: String) : CapabilityQuery() {
        override val condition: HasEntries.() -> Boolean = { containsClass(className) }
    }

    data class ServiceExistsCapabilityQuery(val serviceName: String) : CapabilityQuery() {
        override val condition: HasEntries.() -> Boolean = { contains("META-INF/services/$serviceName") }
    }

}

class CapabilitiesGenerator(
    private val scope: String,
    private val resolvedDependenciesLazy: Lazy<Collection<ResolvedDependency>>,
    private val capabilityQueries: Map<String, List<CapabilityQuery>>
) : Generator {

    override fun invoke(): Map<String, Set<String>> {
        val queryCapabilities = mutableMapOf<CapabilityQuery, SortedSet<String>>().apply {
            capabilityQueries.forEach { name, queries ->
                queries.forEach { query ->
                    computeIfAbsent(query, { sortedSetOf() }).add("$scope:$name")
                }
            }
        }
        if (queryCapabilities.isEmpty()) {
            return emptyMap()
        }

        val resolvedDependencies: Collection<ResolvedDependency> = resolvedDependenciesLazy.value
        if (resolvedDependencies.isEmpty()) {
            return emptyMap()
        }

        val percentageLogger = PercentageLogger(
            "Capabilities generator: $scope",
            queryCapabilities.size.toLong() * resolvedDependencies.size
        )

        val resultCapabilities = concurrentMapOf<DependencyNotation, MutableSet<String>>()
        val dependencyUsages = concurrentMapOf<DependencyNotation, Long>()
        val dependencyNumber = AtomicLong(0)
        queryCapabilities.entries.forEach { (query, capabilities) ->
            val condition = query.condition
            resolvedDependencies.forEach { dependency ->
                val hasEntries = CachedArtifactsCollection(dependency.moduleArtifacts.map(ResolvedArtifact::getFile))
                if (condition(hasEntries)) {
                    val notation = dependency.notation.withOnlyGroupAndModule()
                    resultCapabilities.computeIfAbsent(notation, { concurrentSetOf() }).addAll(capabilities)
                    dependencyUsages.computeIfAbsent(notation, ::getUsages)
                }
                percentageLogger.log(dependencyNumber.incrementAndGet())
            }
        }

        return mutableMapOf<String, Set<String>>().apply {
            resultCapabilities.asSequence()
                .filter { (dependencyUsages[it.key] ?: 0L) >= 10L }
                .sortedByDescending { dependencyUsages[it.key] ?: 0L }
                .forEach {
                    put(it.key.toString(), it.value.toSortedSet())
                }
        }
    }

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

val usagesCacheFile = buildCommittedCacheDir.resolve("usages.json")
val allUsages: SortedMap<String, UsagesInfo> by lazy {
    val map = if (usagesCacheFile.isFile) {
        val type = object : TypeProvider<SortedMap<String, UsagesInfo>>() {}
        type.readJson(usagesCacheFile)!!
    } else {
        sortedMapOf()
    }

    val currentTimeMillis = currentTimeMillis()
    mapOf(
        7 to 1,
        30 to 5,
        6 * 30 to 5,
        12 * 30 to 10
    ).forEach { days, maxCount ->
        val expirationMillis = Duration.ofDays(days.toLong()).toMillis()
        val removedCounter = AtomicInteger(0)
        map.entries.toList()
            .sortedBy { it.value.timestamp }
            .forEach { (key, value) ->
                val isStale = value.timestamp < currentTimeMillis - expirationMillis
                if (isStale && removedCounter.incrementAndGet() <= maxCount) {
                    map.remove(key)
                }
            }
    }

    if (map.isNotEmpty() && map.values.all { it.usages == 0L }) {
        throw IllegalStateException("All usages equal to 0")
    }

    return@lazy synchronizedSortedMap(map)
}

data class UsagesInfo(val usages: Long, val timestamp: Long = currentTimeMillis())

fun getUsages(notation: DependencyNotation): Long {
    val usagesInfo = allUsages.computeIfAbsent("${notation.group}:${notation.module}") { groupModule ->
        Thread.sleep(500)
        var usagesString: String?
        try {
            val statsScript = Jsoup.parse(URL("https://javalibs.com/artifact/${groupModule.replace(':', '/')}"), 30000)
                .select("script")
                .firstOrNull { it.html().contains("scopePieArray") }
                ?.html()
            if (statsScript == null) {
                logger.warn("No usage info for {}: stats script not found", groupModule)
                usagesString = null
            } else {
                usagesString = statsScript.replaceFirst(
                    Regex("[\\s\\S]*\\bvar scopePieArray\\s*=\\s*eval\\(\"[^)\\w]+compile[^)\\w]+(\\d+)[\\s\\S]*"),
                    "$1"
                )
                if (usagesString == statsScript) {
                    usagesString = statsScript.replaceFirst(
                        Regex("[\\s\\S]*\\bvar scopePieArray\\s*=\\s*\\W+compile\\W+(\\d+)[\\s\\S]*"),
                        "$1"
                    )
                    if (usagesString == statsScript) {
                        logger.warn(
                            "No usage info for {}: stats script doesn't have 'compile' usages info",
                            groupModule
                        )
                        usagesString = null
                    }
                }
            }
        } catch (e: HttpStatusException) {
            if (e.statusCode == 404) {
                logger.warn("No usage info for {}: artifact not found", groupModule)
                usagesString = null
            } else {
                throw e
            }
        }

        var usages = usagesString?.toLongOrNull()
        if (usages == null) {
            logger.warn("No usage info for {}: usagesString='{}'", groupModule, usagesString)
            usages = 0
        } else {
            logger.info("Usages for {}: {}", groupModule, usages)
        }

        val result = UsagesInfo(usages)
        synchronized(allUsages) {
            allUsages.writeJsonTo(usagesCacheFile.createParentDirectories(), JSON_DATA_FORMAT.withIndention())
        }
        return@computeIfAbsent result
    }

    return usagesInfo.usages
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

tasks.register("commitGeneratedFiles") {
    doLast {
        vcsOperations.commit("[skip ci] generated files")
        didWork = true
    }
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

dependencies {
    compileOnly(gradleApi())
    compileOnly("name.remal:gradle-plugins:1.1.1")
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

class PercentageLogger(private val name: String, private val total: Long) {

    constructor(name: String, total: Int) : this(name, total.toLong())

    companion object {
        private val logger = getGradleLogger(PercentageLogger::class.java)
        private val periodNanos = Duration.ofSeconds(15).toNanos()
    }

    private val format = "%s: %3d%% (%${total.toString().length}d / %d)"

    private var lastTimestampNanos: Long? = null
    private var latestLoggedPercent: Int = 0

    fun log(current: Long) {
        val percent = if (current == total) {
            100
        } else {
            floor((current.toDouble() / total) * 100).toInt()
        }

        if (latestLoggedPercent == percent) {
            return
        }

        val timestampNanos = nanoTime()
        if (percent < 100) {
            if (lastTimestampNanos == null) {
                lastTimestampNanos = timestampNanos
                return
            } else if (lastTimestampNanos!! >= timestampNanos - periodNanos) {
                return
            }
        }

        logger.lifecycle(format.format(name, percent, current, total))

        latestLoggedPercent = percent
        lastTimestampNanos = timestampNanos
    }

    fun log(current: Int) = log(current.toLong())

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

fun Task.disableTask() {
    enabled = false
    onlyIf { false }
    setDependsOn(emptyList<Any>())

    val inputs = this.inputs
    inputs.javaClass.unwrapGradleGenerated()
        .getDeclaredField("registeredFileProperties")
        .apply { isAccessible = true }
        .get(inputs)
        .uncheckedCast<MutableIterable<Any>>()
        .iterator()
        .run {
            while (hasNext()) {
                next()
                remove()
            }
        }
}
